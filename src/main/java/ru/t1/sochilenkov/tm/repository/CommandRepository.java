package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.api.repository.ICommandRepository;
import ru.t1.sochilenkov.tm.constant.ArgumentConstant;
import ru.t1.sochilenkov.tm.constant.CommandConstant;
import ru.t1.sochilenkov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandConstant.HELP, ArgumentConstant.HELP, "Show help info");

    public static final Command VERSION = new Command(CommandConstant.VERSION, ArgumentConstant.VERSION, "Show version info");

    public static final Command ABOUT = new Command(CommandConstant.ABOUT, ArgumentConstant.ABOUT, "Show developer info");

    public static final Command INFO = new Command(CommandConstant.INFO, ArgumentConstant.INFO, "Show system info");

    public static final Command PROJECT_LIST = new Command(CommandConstant.PROJECT_LIST, null, "Show project list");

    public static final Command PROJECT_CREATE = new Command(CommandConstant.PROJECT_CREATE, null, "Create new project");

    public static final Command PROJECT_CLEAR = new Command(CommandConstant.PROJECT_CLEAR, null, "Delete all projects");

    public static final Command TASK_LIST = new Command(CommandConstant.TASK_LIST, null, "Show task list");

    public static final Command TASK_CREATE = new Command(CommandConstant.TASK_CREATE, null, "Create new task");

    public static final Command TASK_CLEAR = new Command(CommandConstant.TASK_CLEAR, null, "Delete all tasks");

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(CommandConstant.PROJECT_SHOW_BY_INDEX, null, "Show project by index");

    public static final Command PROJECT_SHOW_BY_ID = new Command(CommandConstant.PROJECT_SHOW_BY_ID, null, "Show project by id");

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(CommandConstant.PROJECT_UPDATE_BY_INDEX, null, "Update project by index");

    public static final Command PROJECT_UPDATE_BY_ID = new Command(CommandConstant.PROJECT_UPDATE_BY_ID, null, "Update project by id");

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(CommandConstant.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index");

    public static final Command PROJECT_REMOVE_BY_ID = new Command(CommandConstant.PROJECT_REMOVE_BY_ID, null, "Remove project by id");

    public static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(CommandConstant.PROJECT_CHANGE_STATUS_BY_INDEX, null, "Manual change project status by index");

    public static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(CommandConstant.PROJECT_CHANGE_STATUS_BY_ID, null, "Manual change project status by id");

    public static final Command PROJECT_START_BY_INDEX = new Command(CommandConstant.PROJECT_START_BY_INDEX, null, "Set project status to 'In progress' by index");

    public static final Command PROJECT_START_BY_ID = new Command(CommandConstant.PROJECT_START_BY_ID, null, "Set project status to 'In progress' by id");

    public static final Command PROJECT_COMPLETE_BY_INDEX = new Command(CommandConstant.PROJECT_COMPLETE_BY_INDEX, null, "Set project status to 'Completed' by index");

    public static final Command PROJECT_COMPLETE_BY_ID = new Command(CommandConstant.PROJECT_COMPLETE_BY_ID, null, "Set project status to 'Completed' by id");

    public static final Command TASK_SHOW_BY_INDEX = new Command(CommandConstant.TASK_SHOW_BY_INDEX, null, "Show task by index");

    public static final Command TASK_SHOW_BY_ID = new Command(CommandConstant.TASK_SHOW_BY_ID, null, "Show task by id");

    public static final Command TASK_UPDATE_BY_INDEX = new Command(CommandConstant.TASK_UPDATE_BY_INDEX, null, "Update task by index");

    public static final Command TASK_UPDATE_BY_ID = new Command(CommandConstant.TASK_UPDATE_BY_ID, null, "Update task by id");

    public static final Command TASK_REMOVE_BY_INDEX = new Command(CommandConstant.TASK_REMOVE_BY_INDEX, null, "Remove task by index");

    public static final Command TASK_REMOVE_BY_ID = new Command(CommandConstant.TASK_REMOVE_BY_ID, null, "Remove task by id");

    public static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(CommandConstant.TASK_CHANGE_STATUS_BY_INDEX, null, "Manual change task status by index");

    public static final Command TASK_CHANGE_STATUS_BY_ID = new Command(CommandConstant.TASK_CHANGE_STATUS_BY_ID, null, "Manual change task status by id");

    public static final Command TASK_START_BY_INDEX = new Command(CommandConstant.TASK_START_BY_INDEX, null, "Set task status to 'In progress' by index");

    public static final Command TASK_START_BY_ID = new Command(CommandConstant.TASK_START_BY_ID, null, "Set task status to 'In progress' by id");

    public static final Command TASK_COMPLETE_BY_INDEX = new Command(CommandConstant.TASK_COMPLETE_BY_INDEX, null, "Set task status to 'Completed' by index");

    public static final Command TASK_COMPLETE_BY_ID = new Command(CommandConstant.TASK_COMPLETE_BY_ID, null, "Set task status to 'Completed' by id");

    public static final Command TASK_BIND_TO_PROJECT = new Command(CommandConstant.TASK_BIND_TO_PROJECT, null, "Bind task to project");

    public static final Command TASK_UNBIND_FROM_PROJECT = new Command(CommandConstant.TASK_UNBIND_FROM_PROJECT, null, "Unbind task from project");

    public static final Command TASK_SHOW_BY_PROJECT_ID = new Command(CommandConstant.TASK_SHOW_BY_PROJECT_ID, null, "Show tasks list by project id");

    public static final Command EXIT = new Command(CommandConstant.EXIT, null, "Close application");

    public static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, EXIT,

            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR, PROJECT_SHOW_BY_INDEX,
            PROJECT_SHOW_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_ID,
            PROJECT_COMPLETE_BY_INDEX, PROJECT_COMPLETE_BY_ID,

            TASK_CREATE, TASK_LIST, TASK_CLEAR, TASK_SHOW_BY_INDEX,
            TASK_SHOW_BY_ID, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID, TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_ID,
            TASK_START_BY_INDEX, TASK_START_BY_ID, TASK_COMPLETE_BY_INDEX, TASK_COMPLETE_BY_ID,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT, TASK_SHOW_BY_PROJECT_ID
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
